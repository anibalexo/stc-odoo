# Aplicación para Integracion ODOO-STC

Aplicación para gestión de información de transacciones comerciales de C.E.M Lojagas.

Este proyecto tiene como objetivo integrar la información entre Odoo y la plataforma del gobierno
STC (Sistema de Trazabilidad Comercial) mediante el uso de Django y Docker.
La idea es permitir la transferencia de datos de forma segura y eficiente, con el fin de
mejorar la eficacia y eficiencia del procesamiento de la información en ambas plataformas.

## Requirementos

- Django 4.1
- Python 3.9
- Docker 20.10

## Instalación

1. Clonar el repositorio y acceder al directorio del proyecto
    ```shell
    git clone git@gitlab.com:anibalexo/stc-odoo.git
    ```
    ```shell
    cd examples
    ```
   
3. Construir las imagenes docker
    ```shell
    docker-compose build
    ```

4. Levantar los contenedores docker
    ```shell
    docker-compose up
    ```

5. Acceder mediante el navegador
   ```web
   http://localhost:8000/
   ```

...

## Contacto

- [Anibal Gonzalez](mailto:aigonzalezpec@gmail.com?subject=[ODOO-STC]%20Source%20Han%20Sans)

