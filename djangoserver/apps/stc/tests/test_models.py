import pytest
from model_bakery import baker


class TestActorModel:

    @pytest.mark.django_db
    def test_str_actor_model(self):
        obj = baker.make('stc.Actor')
        assert obj.__str__() == "{}".format(obj.nombre)


class TestNotificacionErrorModel:

    @pytest.mark.django_db
    def test_str_notificacion_error_model(self):
        obj = baker.make('stc.NotificacionError')
        assert obj.__str__() == "{} - {}".format(obj.tipo, obj.mensaje)


class TestProductoModel:

    @pytest.mark.django_db
    def test_str_producto_model(self):
        obj = baker.make('stc.Producto')
        assert obj.__str__() == "{}".format(obj.nombre)


class TestTransaccionModel:

    @pytest.mark.django_db
    def test_str_transaccion_model(self):
        obj = baker.make('stc.Transaccion')
        assert obj.__str__() == "{} - {} - {}".format(obj.tipo,
                                                      obj.reportador,
                                                      obj.fecha)

