from django.contrib import admin

from .models.actor import Actor
from .models.producto import Producto
from .models.transaccion import Transaccion
from .models.notificacion_error import NotificacionError


class ActorAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'codigo_stc', 'tipo', 'fecha_inicio', 'activo')
    search_fields = ('nombre', 'codigo_stc')
    list_filter = ('activo', 'tipo')
    list_per_page = 20
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')


class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'codigo_stc', 'presentacion', 'peso', 'segmento')
    search_fields = ('nombre', 'codigo_stc')
    list_filter = ('activo', 'presentacion', 'segmento')
    list_per_page = 20
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')


class TransaccionAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'tipo', 'reportador', 'contraparte', 'producto', 'cantidad')
    search_fields = ('tipo', 'reportador__nombre', 'contraparte__nombre')
    list_filter = ('tipo', 'estado')
    list_per_page = 20
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')


class NotificacionErrorAdmin(admin.ModelAdmin):
    list_display = ('pk', 'tipo', 'transaccion', 'fecha')
    search_fields = ('tipo', 'mensaje', 'codigo')
    list_filter = ('tipo',)
    list_per_page = 20
    readonly_fields = ('tipo', 'transaccion', 'mensaje', 'codigo', 'fecha',)


admin.site.register(Actor, ActorAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Transaccion, TransaccionAdmin)
admin.site.register(NotificacionError, NotificacionErrorAdmin)
