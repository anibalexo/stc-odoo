from django.core import validators
from django.core.exceptions import ValidationError


def validar_numero(val):
    validators.RegexValidator(
        r'^\d+$',
        'Este campo solo puede contener números'
    )(val)


def validar_tamanio(atr, tam):
    if len(str(atr)) != tam:
        raise ValidationError(
            "El valor {} debe contener {} caracteres".format(atr, tam)
        )

