from django.db import models


class NotificacionError(models.Model):

    class TipoNotificacionError(models.TextChoices):
        ERROR = 'ERR', 'ERROR'
        ADVERTENCIA = 'ADV', 'ADVERTENCIA'
        INFORMACION = 'INF', 'INFORMACION'

    transaccion = models.ForeignKey('stc.Transaccion', on_delete=models.CASCADE,
                                    related_name='notif_transaccion')
    tipo = models.CharField(max_length=3, choices=TipoNotificacionError.choices)
    mensaje = models.TextField()
    codigo = models.CharField(max_length=80)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'notificacion-error'
        verbose_name_plural = 'notificaciones y errores'

    def __str__(self):
        return "{} - {}".format(self.tipo, self.mensaje)
