from django.db import models

from ..utils.validators import validar_numero


class Producto(models.Model):

    class TipoPresentacion(models.TextChoices):
        ENVASADO = 'ENV', 'ENVASADO'
        GRANEL = 'GRN', 'GRANEL'

    class TipoSegmento(models.TextChoices):
        DOMESTICO = 'DOM', 'DOMESTICO'
        INDUSTRIAL = 'IND', 'INDUSTRIAL'
        AGROINDUSTRIAL = 'AGR', 'AGROINDUSTRIAL'

    nombre = models.CharField(max_length=80, unique=True)
    codigo_stc = models.CharField(max_length=10, unique=True, validators=[validar_numero])
    presentacion = models.CharField(max_length=3, choices=TipoPresentacion.choices,
                                    default=TipoPresentacion.ENVASADO)
    peso = models.IntegerField(default=1)
    segmento = models.CharField(max_length=3, choices=TipoSegmento.choices,
                                default=TipoSegmento.DOMESTICO)
    codigo_erp = models.CharField(max_length=15, help_text="Codigo de producto en ERP")
    activo = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'producto'
        verbose_name_plural = 'productos'

    def __str__(self):
        return self.nombre

