from django.db import models

from ..utils.validators import validar_numero


class Actor(models.Model):

    class TipoActor(models.TextChoices):
        LOJAGAS = 'LG', 'LOJAGAS'
        DISTRIBUIDOR = 'DS', 'DISTRIBUIDOR'
        PETROECUADOR = 'PE', 'PETROECUADOR'

    nombre = models.CharField(max_length=80, unique=True)
    codigo_stc = models.CharField(max_length=10, unique=True, validators=[validar_numero])
    clave_stc = models.CharField(max_length=100)
    tipo = models.CharField(max_length=2, choices=TipoActor.choices, default=TipoActor.LOJAGAS)
    fecha_inicio = models.DateField(help_text="Fecha de inicio de actividades en STC")
    activo = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'actor'
        verbose_name_plural = 'actores'

    def __str__(self):
        return self.nombre
