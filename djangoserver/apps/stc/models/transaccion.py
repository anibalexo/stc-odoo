from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models

from ..utils.validators import validar_numero, validar_tamanio


class Transaccion(models.Model):
    class TipoTransaccion(models.TextChoices):
        RECEPCION = 'REC', 'RECEPCION'
        DESPACHO = 'DES', 'DESPACHO'
        VENTA = 'VEN', 'VENTA'

    class TipoTransporte(models.TextChoices):
        TRANSPORTE = 'TRA', 'TRANSPORTE'
        DUCTO = 'DUC', 'DUCTO'

    class Estado(models.TextChoices):
        ENVIADO = 'ENV', 'ENVIADO'
        PENDIENTE = 'PEN', 'PENDIENTE'
        DEVUELTO = 'DEV', 'DEVUELTO'
        ANULADO = 'ANU', 'ANULADO'

    fecha = models.DateTimeField()
    tipo = models.CharField(max_length=3, choices=TipoTransaccion.choices,
                            default=TipoTransaccion.DESPACHO)
    reportador = models.ForeignKey('stc.Actor', on_delete=models.CASCADE,
                                   related_name='actor_reporta',
                                   help_text="Actor STC que reporta transaccion")
    contraparte = models.ForeignKey('stc.Actor', on_delete=models.CASCADE,
                                    null=True, blank=True,
                                    related_name='actor_contraparte',
                                    help_text="Actor STC contraparte en transaccion")
    transporte = models.CharField(max_length=3, choices=TipoTransporte.choices,
                                  null=True, blank=True)
    guia = models.CharField(max_length=15, null=True, blank=True, validators=[validar_numero])
    dni_chofer = models.CharField(max_length=10, null=True, blank=True,
                                  validators=[validar_numero])
    placa = models.CharField(max_length=7, null=True, blank=True)
    producto = models.ForeignKey('stc.Producto', on_delete=models.CASCADE,
                                 related_name='producto_transaccion')
    cantidad = models.DecimalField(max_digits=5, decimal_places=2,
                                   validators=[MinValueValidator(0.1)])
    codigo_confirmacion = models.IntegerField(default=0)
    estado = models.CharField(max_length=3, choices=Estado.choices,
                              default=Estado.PENDIENTE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'transaccion'
        verbose_name_plural = 'transacciones'

    def validar_actores(self):
        if self.tipo != self.TipoTransaccion.VENTA:
            if not self.reportador or not self.contraparte:
                raise ValidationError("No puede haber reportador o contraparte vacia")
            if self.reportador == self.contraparte:
                raise ValidationError("El reportador y contraparte no pueden ser el mismo")
        else:
            if not self.reportador:
                raise ValidationError("No puede haber reportador vacio")

    def validar_guia(self):
        if self.tipo != self.TipoTransaccion.VENTA:
            if not self.guia:
                raise ValidationError("El numero de guia no puede estar vacio")
            validar_tamanio(self.guia, 15)

    def validar_chofer(self):
        if self.tipo != self.TipoTransaccion.VENTA:
            if not self.dni_chofer:
                raise ValidationError("El campo dni chofer no puede estar vacio")
            validar_tamanio(self.dni_chofer, 10)

    def validar_placa(self):
        if self.tipo != self.TipoTransaccion.VENTA:
            if not self.placa:
                raise ValidationError("El campo placa no puede estar vacio")
            validar_tamanio(self.placa, 7)

    def clean(self):
        self.validar_actores()
        self.validar_guia()
        self.validar_chofer()
        self.validar_placa()
        super(Transaccion, self).clean()

    def __str__(self):
        return "{} - {} - {}".format(self.tipo, self.reportador, self.fecha)

